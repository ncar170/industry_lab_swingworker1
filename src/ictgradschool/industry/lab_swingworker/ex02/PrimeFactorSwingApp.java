package ictgradschool.industry.lab_swingworker.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;




        import java.awt.BorderLayout;
        import java.awt.Cursor;
        import java.awt.Dimension;
        import java.awt.event.ActionEvent;
        import java.awt.event.ActionListener;
import java.util.ArrayList;


import javax.swing.JButton;
        import javax.swing.JComponent;
        import javax.swing.JFrame;
        import javax.swing.JLabel;
        import javax.swing.JPanel;
        import javax.swing.JScrollPane;
        import javax.swing.JTextArea;
        import javax.swing.JTextField;

/**
 * Simple application to calculate the prime factors of a given number N.
 *
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 *
 */
public class PrimeFactorSwingApp extends JPanel{


    private JButton _startBtn;        // Button to start the calculation process.
    private JTextArea _factorValues;  // Component to display the result.



    private class PrimeFactorizationWorker extends SwingWorker<ArrayList<Long>, Void>{

        private JTextField tfN;

        public PrimeFactorizationWorker(JTextField tfN) {
           this.tfN = tfN;
        }

        @Override
        public ArrayList<Long> doInBackground(){

            String strN = tfN.getText().trim();
            long n = 0;

            ArrayList<Long> returnList = new ArrayList<>();

            try {
                n = Long.parseLong(strN);
            } catch(NumberFormatException e) {
                System.out.println(e);
            }


            // Start the computation in the Event Dispatch thread.
            for (long i = 2; i*i <= n; i++) {

                // If i is a factor of N, repeatedly divide it out
                while (n % i == 0) {
                    returnList.add(i);
                    n = n / i;
                }
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
                returnList.add(n);
            }



            return returnList;
        }

        public void done(){
            // Re-enable the Start button.

            // Restore the cursor.
       //     setCursor(Cursor.getDefaultCursor());

            ArrayList<Long> result;

            try{
                result = get();
                for (int i = 0; i < result.size(); i++){
                    _factorValues.append(Long.toString(result.get(i)));
                }
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
            finally {
                _startBtn.setEnabled(true);
            }
        }
    }

    public PrimeFactorSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                PrimeFactorizationWorker worker = new PrimeFactorizationWorker(tfN);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);

                // Set the cursor to busy.
              //  setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                worker.execute();
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500,300));
    }

    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new PrimeFactorSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

